using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "SO/new Pool config", fileName = "PoolConfig")]
public class PoolConfig : ScriptableObject
{
    public List<PoolItem> PoolItems;
}

[System.Serializable]
public class PoolItem
{
    [SerializeField] private int _count;
    [SerializeField] private GameObject _prefab;

    public int Count  //=> _count
    {
        get
        {
            return _count;
        }
    }

    public GameObject Prefab => _prefab;
}

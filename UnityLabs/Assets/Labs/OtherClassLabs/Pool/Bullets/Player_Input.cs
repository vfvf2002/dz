using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Input : MonoBehaviour
{
    [SerializeField] private Camera _mainCamera;
    [SerializeField] private Bullet _bullet;

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            _bullet.BulletActive(GetScreenPoint());
         //   _bullet.BulletForce();
        }
    }

    private Vector3 GetScreenPoint()
    {
        if (Input.GetMouseButton(0))
        {
            Debug.Log(_mainCamera.ScreenToWorldPoint(Input.mousePosition));

            return  _mainCamera.ScreenToWorldPoint(Input.mousePosition);
        }
        return Vector3.zero;
    }
}

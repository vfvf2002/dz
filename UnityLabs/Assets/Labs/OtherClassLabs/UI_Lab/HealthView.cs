using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;


public class HealthView : MonoBehaviour
{
    [SerializeField] private Slider _healthSlider;
    [SerializeField] private Health_1 _health;

    private void OnEnable()
    {
        Health_1.OnHealthChange += UpdateHealth;
    }

    private void OnDisable()
    {
        Health_1.OnHealthChange -= UpdateHealth;
    }

    private void UpdateHealth(float d)
    {
        _healthSlider.value = _health.HealthCount;
    }
}

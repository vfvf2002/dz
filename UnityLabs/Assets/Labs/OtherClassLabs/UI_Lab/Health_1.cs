using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health_1 : MonoBehaviour
{
    [SerializeField] private float _healthCount;
    public float HealthCount => _healthCount;
    public static event System.Action<float> OnHealthChange;

    void Update()
    {
       if(Input.GetKeyDown(KeyCode.E))
       {
            TakeDamage(0.1f);
            OnHealthChange?.Invoke(0.1f);
       }
    }

    public void TakeDamage(float d) =>
        _healthCount -= d;
}

using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class PlayerFollower : MonoBehaviour
{
    [SerializeField] private NavMeshAgent _navMeshAgent;
    [SerializeField] private Transform _playerTransform;

    private void Update()
    {
        if (Vector3.Distance(_playerTransform.position, _navMeshAgent.transform.position) < 4f)
        {
            _navMeshAgent.SetDestination(_playerTransform.position);
        }
    }
}

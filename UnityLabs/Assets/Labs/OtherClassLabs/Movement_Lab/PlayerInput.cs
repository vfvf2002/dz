using UnityEngine;

[RequireComponent(typeof(PlayerMovement))]
public class PlayerInput : MonoBehaviour
{

    [SerializeField] private Camera _mainCamera;
    [SerializeField] private PlayerMovement _playerMovement;
    
    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
            CastRayToTerrain();
    }

    private void CastRayToTerrain()
    {
        Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit)) 
            _playerMovement.Move(hit.point);
    }
}

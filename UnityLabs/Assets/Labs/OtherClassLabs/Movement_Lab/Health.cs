using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    internal static Action<int> OnHealthChange;
    [SerializeField] public int _healthCount;

    public void ApplyDamage(int damage)
    {
        if (_healthCount > 0)
            _healthCount -= damage;
        else
            _healthCount = 0;
    }

    public int GetHealth() => 
        _healthCount;
}

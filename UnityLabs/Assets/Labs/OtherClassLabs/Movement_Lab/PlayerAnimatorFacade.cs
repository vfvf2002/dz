using UnityEngine;

public class PlayerAnimatorFacade : MonoBehaviour
{
    [SerializeField] private Animator _playerAnimator;
    
    private const string PLAYER_SPEED = "Speed";
    private int _playerSpeedHash = Animator.StringToHash(PLAYER_SPEED);

    private const string PLAYER_HEALTH = "Health";
    private int _playerHealthHash = Animator.StringToHash(PLAYER_HEALTH);

    public void SetSpeed(float speed) => 
        _playerAnimator.SetFloat(_playerSpeedHash, speed);

    public void HealthCounter(int health) =>
    _playerAnimator.SetInteger(_playerHealthHash, health);
}
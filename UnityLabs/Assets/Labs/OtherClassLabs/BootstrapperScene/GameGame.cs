using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameStates;

public class GameGame
{
    private GameStateMachine _gameStateMachine;

    public GameGame(IGameStateMachine gameStateMachine)
    {
        _gameStateMachine = (GameStateMachine)gameStateMachine;
    }
}

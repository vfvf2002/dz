using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameStates
{
    public interface IEnterableState
    {
        void OnEnter();
        void OnExit();
    }
}

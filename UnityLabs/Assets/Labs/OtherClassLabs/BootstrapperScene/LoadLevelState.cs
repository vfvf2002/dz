using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameStates;

public class LoadLevelState : IEnterableState
{
    private SceneLoader _sceneLoader;

    public LoadLevelState(SceneLoader sceneLoader) => _sceneLoader = sceneLoader;

    public void OnEnter()
    {
        _sceneLoader.LoadScene(GameConstants.SAMPLE_SCENE);
    }

    public void OnExit()
    {

    }
}


public class SceneLoader
{
    public void LoadScene(int scene)
    {
        SceneManager.LoadScene(GameConstants.SAMPLE_SCENE);
    }
}

public class GameConstants
{
    public const int SAMPLE_SCENE = 0;
}
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FormView : MonoBehaviour
{
    public TMP_InputField NickField;
    [SerializeField] private TMP_InputField _descriptionField;
    [SerializeField] private TMP_Dropdown _raceDropdown;
    [SerializeField] private TMP_Dropdown _classDropdown;
    [SerializeField] private TextMeshProUGUI _statusBarText;

    public void UpdateFields()
    {
        string nick = NickField.text;
        
        string classText = _classDropdown
            .options[_classDropdown.value]
            .text;

        _statusBarText.text = null;
    }

    public void EmptyNameWarning()
    {
        _statusBarText.text = "Nickname field is empty!";
    }
}

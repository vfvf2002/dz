using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emmit : IStrategy
{
    public void Perform(Transform transform)
    {
        ParticleSystem particleSystem = transform.GetComponent<ParticleSystem>();
        particleSystem.Play();
    }
}